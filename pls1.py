# Auteurs: Basile Musquer, Marius Le Chapelier
# Sorbonne Université
# MADMC Projet

import matplotlib.pyplot as plt
import argparse
import numpy as np

from input import getParameters
from function import voisinnage, getObjectiv, generateRandomSolution, MAJ, dominate

def plotBicriteria(non_dom_points):
    '''
        Plot a set of non dominated points in bicriteria
    '''
    plt.plot(non_dom_points[0],non_dom_points[1], '.')
    plt.show()
    return 0

# ******************************************************************************
#                               PLS 1
# ******************************************************************************
def pls1(W,items, Vx=voisinnage, plot= False):
    #****** Sort item by weight:
    items = np.array(sorted(items, key=lambda x: x[0])) # Trie du plus léger au plus lourd

    items_weight = items[:,0]
    objectives = items[:,1:]
    list_item = set(range(len(items))) # from 1 to 100, index 0 is for the size of the bag
    space = []
    solutions = []

    #****** Gen solution initiale :
    x0 = generateRandomSolution(W, items)
    Xe = [x0] # Ensemble de solutions efficaces
    P = [x0] # Pop init = 1 solution générée aléatoirement
    Pa = []

    #******* Plot
    if plot:
        fig, axs = plt.subplots(1,2)
        points = []
        points.append(len(P))

    #******* MAIN Loop
    print("PLS in Progress...")
    # Ordre de grandeur: taille pop P jusqu'à une centaine, nb iter ~ 10
    while len(P) != 0:
        for p in P:
            for v in Vx(W, items_weight, p, list(set(list_item) - set(p))):
                if (dominate(p,v,objectives) != 1):
                    res, Xe = MAJ(Xe,v,objectives)
                    if(res): 
                        _, Pa = MAJ(Pa,v,objectives) ## pas besoin de passer par MAJ ? on ajoute juste dans Pa car 
                                                     ## ce qui n'est pas dominé dans Xe ne le sera pas dans Pa
        P = Pa
        # Tant que Xe = P, un nouveau front est trouvé
        print("(len:Xe, len:P) : ", (len(Xe),len(P)))
        if plot:
            points.append(len(P))
        Pa = []
    
    for s in Xe:
        space.append(getObjectiv(s, objectives))
        solutions.append(s)

    if plot:
        space = np.array(sorted(space, key=lambda x: x[0]))
        fig.suptitle("PLS V1")
        fig.tight_layout(pad=4.0)
        axs[0].scatter(np.array(space)[:,0], np.array(space)[:,1])
        axs[0].set_title("Front de pareto")
        axs[0].set_xlabel("Objectif 1")
        axs[0].set_ylabel("Objectif 2")
        axs[0].set_yticks(np.linspace(33000, 43000, 6))
        axs[0].set_xticks(np.linspace(34000, 42000, 5))

        axs[1].plot(points)
        axs[1].set_title("Evolution de la taille de P")
        axs[1].set_ylabel("P size")
        axs[1].set_xlabel("nb_iteration")
        axs[1].set_xticks(range(1,int(len(points)/5)))
        plt.show()
    return space, solutions

# ******************************************************************************
#                               MAIN
# ******************************************************************************
if __name__ == '__main__':
    #### data project : 
    parser = argparse.ArgumentParser()
    parser.add_argument("--size", help="Number of agents of instance", default="200", type=str, required=False)
    parser.add_argument("--batch", help="Number of the batch", default="999", type=str, required=False)
    parser.add_argument("--nitems", help="Number of items from the batch", default="20", type=int, required=False)
    parser.add_argument("--nobj", help="Number of objectives", default="3", type=int, required=False)

    
    args = parser.parse_args()
    W, items = getParameters(args.size, args.batch, args.nitems, args.nobj)
    pls1(W, items)