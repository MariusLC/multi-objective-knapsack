## Auteurs
Auteurs: Basile Musquer, Marius Le Chapelier  
Encadrants: Patrice Perny, Thibaut Lust, Olivier Spanjaard

## Description
Name: Multi Objective Knapsack  
Date: 20 janvier 2022  
Statut: Achevé

Ce projet a été mené dans le cadre de l'UE MADMC du master 2 ANDROIDE à Sorbonne Université.

Il est question d'implémenter d'utiliser l'élicitation incrémentale et une recherche locale pour résoudre le problème du sac à dos multi-objectifs.

Deux procédures ont été implémentées:
1. RBLS: combine recherche locale et élicitation incrémentale
2. PLS-EI: effectue d'abord une recherche locale puis applique l'élicitation incrémentale sur le résultat de la recerche.

## Installation
Système Linux de préférence  
Testé sous environnement conda  
Voir le fichier requirements.txt fournit

## Usage
Pour lancer la procédure RBLS:
python rbls.py

***
Pour lancer la procédure PLS-EI:
python incremental_elicitation.py

***
Les deux procédures possèdent un ensemble de 6 paramètres:
1. --func :    fonction d'aggrégation 0:WS, 1:OWA
2. --nexec:    nombre de fois que la procédure est exécutée
3. --nobj:     nombre de critères considéré (dans la limite de ceux du batch)
4. --nitems:   nombre d'objets tirés dans le batch
5. --size:     nombre d'objets total du batch
6. --batch:    numéro du batch

Les paramètres 'size' et 'batch' permettent d'identifier le fichier de données  
Valeurs par défaut utilisées pour les tests:  
--nexec 20  |   --nobj 4   |   --nitems 20  |   --size 200  |   --batch 999

## Visuals
Les résultats de chaque batch d'exécution sont sockés dans le dossier log/ sous le format .json

Pour afficher les résultats:  
Dans le fichier polt.py, indiquer dans le main les chemins des fichiers logs

Lancer plot.py

Les graphiques sont sauvegardés dans le dossier plot/




