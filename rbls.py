# Auteurs: Basile Musquer, Marius Le Chapelier
# Sorbonne Université
# MADMC Projet

import argparse
import numpy as np
import time
import datetime
import json

from incremental_elicitation import rollout
from input import getParameters

from function import voisinnage, getObjectiv, generateInitialSolution, MAJ, addList
from Decideur import Decideur

# ******************************************************************************
#                               RBLS
# ******************************************************************************
def rbls(W, items, decideur, Vx=voisinnage):
    MMRs = []
    count = 0
    #****** Sort item by weight:
    items = np.array(sorted(items, key=lambda x: x[0])) # Trie du plus léger au plus lourd

    items_weight = items[:,0]
    objectives = items[:,1:]
    list_item = set(range(len(items))) # from 1 to 100, index 0 is for the size of the bag

    #****** Gen solution initiale :
    x = generateInitialSolution(W, items_weight, objectives)
    Xe = [x]

    while(True):
        X = []
        X = Vx(W, items_weight, x, list(set(list_item) - set(x)))
        for v in X:
            _ , Xe = MAJ(Xe,v,objectives)

        opti_x, opti_fwx, opti_allocation, exec_x, exec_fwx, new_x, nqueries, MMR = rollout(W,items_weight,objectives, [getObjectiv(x, objectives) for x in Xe], Xe, decideur)
        
        MMRs = addList(MMRs,MMR)
        count+=1
        if(new_x == x):
            #****** Format data
            opti_allocation = sorted([i for i in range(len(opti_allocation)) if opti_allocation[i]==1])
            return opti_x, round(opti_fwx,3), opti_allocation, exec_x.tolist(), round(exec_fwx,3), sorted(x), nqueries, [mmr/count for mmr in MMRs]
        else:
            x = new_x

# ******************************************************************************
#                               MAIN
# ******************************************************************************
if __name__ == '__main__':
    #*********** Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--func", help="Type of Agreg function: 0 WS, 1 OWA", default=1, type=int, required=False)
    parser.add_argument("--size", help="Number of agents of instance", default="200", type=str, required=False)
    parser.add_argument("--batch", help="Number of the batch", default="999", type=str, required=False)
    parser.add_argument("--nitems", help="Number of items from the batch", default=20, type=int, required=False)
    parser.add_argument("--nobj", help="Number of objectives", default=4, type=int, required=False)
    parser.add_argument("--nexec", help="Number of simulations", default=1, type=int, required=False)

    algo_string = "RBLS"
    args = parser.parse_args()
    W, items = getParameters(args.size, args.batch, args.nitems, args.nobj)
    decideur = Decideur(args.nobj, args.func) # OWA: 1   |   SW: 0
    
    fun_string = ""
    if args.func == 0:
        fun_string = "WS"
        print("[Aggregation function]: Weighted Sum")
    else:
        fun_string = "OWA"
        print("[Aggregation function]: OWA")

    #************** Data Structure
    data = {
        "opti_x": 0,
        "opti_fwx": 0,
        "opti_x_allocation": 0,
        "exec_x": 0,
        "exec_fwx": 0,
        "exec_x_allocation": 0,
        "Number of Questions": 0,
        "MMR": 0,
        "Duration (s)": 0
    }

    #************** Execution
    filename = 'log/'+algo_string+"-"+fun_string+"-"+str(args.nexec)+"_"+datetime.datetime.now().strftime("%d-%b-%H-%M-%S")+'.json'
    for i in range(args.nexec):
        decideur.redrawWeights()
        decideur.resetNbQueries()

        print("------Execution", i)
        start_time = time.time()

        samples = rbls(W, items, decideur)

        time_exec = time.time() - start_time 
        print("------time exec = ", time_exec)
        print()

        samples = list(samples)
        samples.append(time_exec)
        for k,key in zip(range(len(samples)),data.keys()):
            data[key] = samples[k]

        with open(filename, 'a') as f:
            json.dump(data, f)
            f.write("\n")