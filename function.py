# Auteurs: Basile Musquer, Marius Le Chapelier
# Sorbonne Université
# MADMC Projet

import math
import numpy as np
import random
# ******************************************************************************
#                               Functions
# ******************************************************************************
def addList(l1, l2):
    if len(l1) > len(l2):
        l2 += [0]*(len(l1)-len(l2))
    else:
        l1 += [0]*(len(l2)-len(l1))
    return (np.add(l1,l2)).tolist()
    
def generateInitialSolution(W, w, u):
    '''
        W : poids maximal du sac
        w: poids des objets
        u: vecteur utilités des objets (dim = nb critères)
        Return: une solution qui maximise la somme arithmétique
                    forme: [indx des objets de la solutions]
                    Note: les index sont triés dans l'ordre du poids
    '''
    # Compute arithmetic mean for each item
    am_items = []
    for i in range(len(u)):
        am_items.append([i,np.sum(u[i])/len(u[i]),w[i]])

    # Trie de la meilleure moyenne à la pire
    am_items = np.array(sorted(am_items, key=lambda x: x[1]))[::-1]

    initial_solution = []
    w_left = W
    for k in am_items:
        if(w[int(k[0])] <= w_left):
            w_left -= w[int(k[0])]
            initial_solution.append(int(k[0]))
        if w_left == 0:
            break
    return initial_solution
    
def generateRandomSolution(W, items):
    '''
        W : poids maximal du sac
        items: liste des items possibles
        Return: une solution
    '''
    shuffleList = list(range(len(items))) # List of indexes from 0 to len(items)
    random.shuffle(shuffleList)

    x = []
    w_left = W # Poids restant dans le sac

    for item in shuffleList: # On prend les index des objets dans l'ordre de la liste mélangée
        carac = items[item]
        if(carac[0] <= w_left): # S'il reste de la place dans le sac
            w_left -= carac[0]  # alourdir le sac du poid de l'objet
            x.append(item)    # ajouter l'objet
        else:
            break
    return x

def getObjectiv(soluce, objectives):
    return np.sum(objectives[soluce], axis=0)

def dominate(x, y, objectives):
    '''
        Return:
            0 si x et y sont non comparables
            1 si x domine strictement y
            2 si x est dominé par y
    '''
    zx = getObjectiv(x,objectives)
    zy = getObjectiv(y,objectives)

    ## Maximisation bicritères
    # if zy[0] < zx[0] and zy[1] < zx[1]:
    #     return 1
    # elif zx[0] <= zy[0] and zx[1] <= zy[1]:
    #     return 2
    # return 0

    # Maximisation multicritère
    nb_obj_x_dom = 0
    nb_obj_y_dom = 0
    i = 0
    while(i<len(zx) and (nb_obj_x_dom == 0 or nb_obj_y_dom == 0)):
        if zx[i] > zy[i]:
            nb_obj_x_dom += 1
        if zy[i] > zx[i] :
            nb_obj_y_dom += 1
        i += 1

    if (nb_obj_x_dom > 0 and nb_obj_y_dom > 0): # pas comparable
        return 0
    elif nb_obj_x_dom == 0: # y domine x 
        return 2
    else: # x domine y
        return 1


# ******************************************************************************
#                               MAJ, Voisinnage
# ******************************************************************************
def MAJ(listSoluce, x, objectives):
    """
        x: nouvelle solution
        listSoluce: solutions déjà existantes
        objectives: valeur des objectifs des élément composant les solutions
    """
    newListSoluce = listSoluce.copy()
    for y in listSoluce:
        res_dom = dominate(x,y, objectives)
        if( res_dom == 1): # la nouvelle solution x domine une solution existante y
            newListSoluce.remove(y)
        elif(res_dom == 2):
            return False, listSoluce # la nouvelle solution x est dominée par une solution existante y
        # Dernier cas: dominate() == 0 , la nouvelle solution x et la solution y existante ne se domine pas mutuellement
    newListSoluce.append(x)

    return True, newListSoluce


def voisinnage(W, items_weight, soluce, anti_soluce):
    wl = W - np.sum(items_weight[soluce])
    lVoisins = []
    for i in soluce: # For each item in the solution
        for j in anti_soluce: 
            if (wl < items_weight[j] - items_weight[i]): # remplacer i par j dépasserai le poids max
                break
            # if(j>i): # Approximation, s'il reste un peu de place dans le sac, il est possible que l'objet i+1 rentre
            #     print("break")
            #     break
            voisin = soluce.copy()
            voisin_anti = anti_soluce.copy()
            voisin_anti.remove(j)
            voisin.remove(i)
            voisin.append(j)
            wl_v = wl + items_weight[i] - items_weight[j]

            # If possible: Complete randomly with items
            while(wl_v > items_weight[voisin_anti[0]]):
                ind = np.max(np.argwhere(np.array(items_weight[voisin_anti]) < wl_v)) ## couteux peut être ça ?
                r = random.randint(0,ind)
                wl_v -= items_weight[voisin_anti[r]]
                voisin.append(voisin_anti[r])
                del voisin_anti[r]

            lVoisins.append(voisin)
    return lVoisins

def choice_solutions(solutions):
    sol_1 = solutions[random.randint(0, math.floor(len(solutions)/2))]
    sol_2 = solutions[random.randint(math.ceil(len(solutions)/2), len(solutions)-1)]
    return sol_1, sol_2