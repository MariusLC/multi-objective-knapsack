import gurobipy as gp
from gurobipy import *

def optWeightedSum(prices, weights, utilities):
	"""
		price: vecteur poids de la fonction d'aggrégation, dim=(p,)
		utilities: 2D vecteur des utilités des objets pour chaque critère dim(i,p)
		weights: vecteur poids des objets, dim=(i,)
	"""
	W = sum(weights)/2
	model = gp.Model("WeightedSum")
	model.setParam('OutputFlag', False)
	model.setParam("IntegralityFocus", 1)

	allocation = []
	for i in range(len(weights)):
		allocation.append(model.addVar(lb=0.0, ub=1.0, vtype=GRB.BINARY, name="x"+str(i)))

	model.addConstr(gp.quicksum([weights[i]*allocation[i] for i in range(len(weights))]) <= W, name='size')

	model.setObjective(gp.quicksum(	[prices[p]*gp.quicksum([utilities[i][p]*allocation[i] for i in range(len(weights))]) for p in range(len(prices))]	), GRB.MAXIMIZE)
	model.write('lp/WS.lp')
	model.optimize()

	res = model.ObjVal
	final_allocation = []
	for v in model.getVars():
		final_allocation.append(v.x)

	criteria = []
	for p in range(len(prices)):
		criteria.append(sum([utilities[i][p]*final_allocation[i] for i in range(len(utilities))]))
	return res, final_allocation, criteria

def optOWA(prices, weights, utilities, verbose=False):
	"""
		price: vecteur prix des objets, dim=(i,)
		utilities: 2D vecteur des utilités des objets pour chaque critère dim(i,p)
		weights: vecteur poids de la fonction d'aggrégation, dim=(p,)
	"""
	W = sum(weights)/2
	model = gp.Model("OWA")
	model.setParam('OutputFlag', False)
	model.setParam('DualReductions', 0)

	# Define Knapsack problem
	allocation = []
	for i in range(len(weights)):
		allocation.append(model.addVar(lb=0.0, ub=1.0, vtype=GRB.BINARY, name="x"+str(i)))

	model.addConstr(gp.quicksum([weights[i]*allocation[i] for i in range(len(weights))]) <= W, name='size')

	# Define OWA optimization
	M = 100000
	z = []
	for k in range(len(prices)):
		tmp = []
		for i in range(len(prices)):
			tmp.append(model.addVar(lb=0.0, ub=1.0, vtype=GRB.BINARY, name='z'+str(k)+str(i)))
		z.append(tmp)

	r = []
	for k in range(len(prices)):
		r.append(model.addVar(vtype=GRB.CONTINUOUS, name='r'+str(k)))

	for k in range(len(prices)):
		model.addConstr(gp.quicksum(z[k]) <= k, name='sumZ'+str(k))

	for k in range(len(prices)):
		for i in range(len(prices)):
			model.addConstr(r[k] - gp.quicksum([utilities[j][i]*allocation[j] for j in range(len(weights))]) <= M*z[k][i], name='y'+str(k)+str(i))

	model.setObjective(gp.quicksum([prices[k]*r[k] for k in range(len(prices))]) ,GRB.MAXIMIZE)
	model.write('lp/OWA.lp')
	model.optimize()

	res = round(model.ObjVal,6)
	final_allocation = []
	for v in model.getVars():
		if verbose:
			print(v.VarName,":", round(v.x,6))
		if v.VarName[0] == 'x':
			final_allocation.append(abs(v.x))

	criteria = []
	for p in range(len(prices)):
		criteria.append(sum([utilities[i][p]*final_allocation[i] for i in range(len(utilities))]))
	return res, final_allocation, criteria

if __name__ == "__main__":
	prices = [0.5,0.3,0.2] # 3 critères
	weights = [5,5,4,6]	   # 4 objets
	utilities = [
		[3,7,2],
		[7,1,4],
		[4,8,3],
		[6,2,3]
	]

	print(optWeightedSum(prices,weights, utilities))
	# Solution optimale attendue: (0,1,1,0) -> [11,7,9]
	#							  7+4*0.5 + 4+3*0.3 + 1+8*0.2= 9.4

	print(optOWA(prices,weights,utilities))
	# Solution optimale: (1,0,1,0) -> [7,5,15] = 0.5*15+0.3*7+0.2*5 = 10.6
