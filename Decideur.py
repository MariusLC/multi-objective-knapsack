import numpy as np 
import random
from aggregation_functions import weighted_sum, OWA


w1 = np.array([0.3, 0.3, 0.2, 0.1, 0.05, 0.05])
w2 = np.array([0.1, 0.2, 0.4, 0.2, 0.05, 0.05])
w3 = np.array([0.05, 0.05, 0.1, 0.1, 0.3, 0.4])

class Decideur:
	def __init__(self, nb_objectives, fnc):
		self.nobj = nb_objectives

		# aggregation_function
		self.agg_f = weighted_sum if fnc==0 else OWA

		self.nb_queries = 0

	def resetNbQueries(self):
		self.nb_queries = 0

	def redrawWeights(self):
		self.hidden_weights = np.array([random.random() for i in range(self.nobj)])
		self.hidden_weights /= sum(self.hidden_weights)
		if self.agg_f == OWA:
			self.hidden_weights = sorted(self.hidden_weights, reverse=True)

	def ask_query(self, sol_1, sol_2):
		self.nb_queries += 1
		res_1 = self.agg_f(self.hidden_weights, sol_1)
		res_2 = self.agg_f(self.hidden_weights, sol_2)
		if res_1 > res_2 : # maximisation
			return sol_1, sol_2
		else :
			return sol_2, sol_1

	def calculate_exact_MMR(self, non_dom_solutions):
		nb_objectives = len(non_dom_solutions[0])
		nb_solutions = len(non_dom_solutions)
		MR = -float("inf")
		y_MR = 0
		MMR = float("inf")
		x_min = 0
		y_MMR = 0
		x_MMR = 0
		res = 0
		mr_list = []
		for x in range(nb_solutions):
			MR = -float("inf")
			weights_sol_y = None
			y_MR = None
			for y in range(nb_solutions):
				if x != y :

					res = self.agg_f(self.hidden_weights, non_dom_solutions[y]) - self.agg_f(self.hidden_weights, non_dom_solutions[x])

					# Compile MR
					if res > MR :
						# print("MR")
						# print("x = "+str(x))
						# print("y = "+str(y))
						y_MR = y
						MR = res

			# Compile MR
			mr_list.append(MR)
			if MR < MMR:
				# print("MMR")
				# print("x = "+str(x))
				# print("y = "+str(y))
				x_MMR = x
				y_MMR = y_MR
				MMR = MR

		print("EXACT DECIDEUR")
		print("opti x = ", x_MMR)
		print("opti y = ", y_MMR)
		print("opti x = ", non_dom_solutions[x_MMR])
		print("opti y = ", non_dom_solutions[y_MMR])
		print("fw_x = ", self.agg_f(self.hidden_weights, non_dom_solutions[x_MMR]))
		print("MMR = ", MMR)
		return MMR